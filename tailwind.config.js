/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    screens:{
      'xsm' : '340px',
      'sm' : '640px',
      'md' :	'768px',
      'lg' :	'1024px',
      'xl' : '1280px',
      '2xl' : '1536px',
    },
    extend: {
      colors: {
        'blue' : '#5940F3',
        'lg': 'linear-gradient(0deg, #120085, #120085)',
      },
      letterSpacing: {
        tightest: '-.075em',
        tighter: '-.05em',
        tight: '-.025em',
        normal: '0',
        wide: '.025em',
        wider: '.05em',
        widest: '.1em',
        widests: '.25em',
    },
    fontSize:{
      '17': '18px',
    },
    width: {
      'sw' : '200px',
      'wide-width': '1020px',
      'width': '850px',
      'wide' : '449px',
      'wider': '500px',
      'c-width': '305px'
    },
    height: {
      'height' : '600px',
      'hgt' : '980px',
      'c-height': '301px',
      'f-height': '430px',
      'small': '149px',
    },
    lineHeight: {
      'loose': '28px',
      'more-loose': '56px',
    }
  },
  plugins: [],
}
}


