import React from 'react'
import Image from 'next/image'

const Blog = () => {
    return (
        <>
            <div className='lg:my-12 text-center lg:p-5 xsm:px-6 xsm:my-16'>
                <div>
                    <h2 className='lg:text-4xl xsm:text-2xl leading-more-loose font-serif'>
                        Noah's Ark Sports Science College - Tourism
                    </h2>
                </div>
                <div className='lg:w-wide-width xsm:w-full lg:mt-6 lg:mx-auto xsm:mt-6'>
                    <p className=' font-sans font-light leading-loose text-center'>
                        Pokhara is situated in the very beautiful heart of Nepal and offers many opportunities for tourism.
                        It boasts seven lakes, is surrounded by hills and is located at the edge of the Annapurna Himalaya
                        reaching 8000m in height. Tourism opportunities include boating, cycling, homestay in traditional
                        village, paragliding, ultralight flights, zip were, bungee jumping and, of course, trekking.
                    </p>
                </div>
            </div>
            <div className='bg-[#F9F9F9]'>
                <div className='lg:flex'>
                    <div className='lg:px-0 lg:block xsm:px-6 xsm:hidden'>
                        <Image
                            src="/9.png"
                            alt=""
                            width={820}
                            height={500}
                            className=""
                        />
                    </div>
                    <div>
                        <div className='lg:w-wide lg:h-small xsm:w-full lg:mt-6 lg:mx-24 lg:p-10 xsm:p-6'>
                            <h2 className='lg:text-4xl xsm:text-2xl lg:text-start xsm:text-center leading-more-loose font-serif'>
                                Noah's Ark Agro Tourism & Homestays
                            </h2>
                        </div>
                        <div className='lg:px-0 lg:hidden xsm:px-6 lg:mb-0 xsm:mb-3'>
                            <Image
                                src="/9.png"
                                alt=""
                                width={820}
                                height={500}
                                className=""
                            />
                        </div>
                        <div className='lg:w-wider lg:h-small xsm:w-full lg:mx-24 lg:py-4 lg:px-10 xsm:px-6'>
                            <p className='lg:text-start xsm:text-center text-base font-sans font-light leading-loose'>
                                Pokhara is situated in the very beautiful heart of Nepal and offers many opportunities for tourism.
                                It boasts seven lakes, is surrounded by hills and is located at the edge of the Annapurna Himalaya
                                reaching 8000m in height.
                            </p>
                            <p className='lg:text-start xsm:text-center text-base font-sans font-light leading-loose lg:pt-5 xsm:pt-4'>
                                Tourism opportunities include boating, cycling, homestay in traditional
                                village, paragliding, ultralight flights, zip were, bungee jumping and, of course, trekking.
                            </p>
                        </div>
                    </div>
                </div>
                <div className='lg:flex lg:px-0 xsm:px-6'>
                    <div>
                        <div className='lg:w-wider xsm:w-full lg:mt-20 lg:mx-40 lg:py-6 xsm:mt-10 xsm:py-6 text-center'>
                            <h2 className='lg:text-4xl xsm:text-2xl lg:text-start xsm:text-center leading-more-loose font-serif'>Pokhara International Airport</h2>
                        </div>
                        <div className='lg:mt-0 lg:mb-0 xsm:mt-4 lg:hidden xsm:mb-4'>
                            <Image
                                src="/10.png"
                                alt=""
                                width={820}
                                height={500}
                                className=""
                            />
                        </div>
                        <div className='lg:w-wider lg:mx-32 lg:px-10 lg:pb-0 xsm:pb-10'>
                            <p className=' lg:text-start xsm:text-center text-base font-sans font-light leading-loose'>
                                Pokhara is situated in the very beautiful heart of Nepal and offers many opportunities for tourism.
                                It boasts seven lakes, is surrounded by hills and is located at the edge of the Annapurna Himalaya
                                reaching 8000m in height.
                            </p>
                            <p className='lg:text-start xsm:text-center text-base font-sans font-light leading-loose lg:pt-5'>
                                Tourism opportunities include boating, cycling, homestay in traditional
                                village, paragliding, ultralight flights, zip were, bungee jumping and, of course, trekking.
                            </p>
                        </div>
                    </div>
                    <div className='lg:mt-0 lg:block xsm:hidden'>
                        <Image
                            src="/10.png"
                            alt=""
                            width={820}
                            height={500}
                            className=""
                        />
                    </div>
                </div>
            </div>
        </>
    )
}

export default Blog