import React from 'react'
import Image from 'next/image'
const About = () => {
    return (
        <>
            <div className='md:flex'>
                <div className='z-10 lg:block xsm:hidden'>
                    <Image
                        src="/3.png"
                        alt=""
                        width={720}
                        height={439}
                        className="lg:my-20 lg:mx-36 xsm:mt-24 xsm:px-6"
                    />
                </div>
                <div className='lg:w-width lg:h-height xsm:w-full bg-[#dbe3f2]'>
                    <div className='lg:w-wide lg:h-small lg:mt-16 lg:mx-24 lg:p-10 lg:text-start xsm:w-full xsm:mt-24 xsm:py-10 xsm:text-center'>
                        <h2 className='md:text-4xl xsm:text-3xl leading-more-loose font-serif'>Who was the original Noah?</h2>
                    </div>
                    <div className='lg:w-wider lg:h-small xsm:w-full lg:mx-24 lg:py-6 lg:px-10 lg:text-start xsm:px-10 xsm:text-center'>
                        <p className='text-base font-sans font-light leading-loose'>Noah lived around 4400* years ago somewhere in the Middle East. The world was totally evil
                            and corrupt at this time so God decided to destroy all of Mankind with a flood. However, since
                            Noah was a very good person, God decided to save him and his family in an Ark that Noah built.
                        </p>
                    </div>
                    <div className='z-10 lg:hidden'>
                        <Image
                            src="/3.png"
                            alt=""
                            width={720}
                            height={439}
                            className="lg:my-20 lg:mx-32 xsm:mt-10 xsm:px-6"
                        />
                    </div>
                    <div className='w-wider h-small lg:mt-10 lg:mx-24 lg:p-10 xsm:mx-32 xsm:pt-10'>
                        <button className='text-white w-36 h-14 bg-blue mr-4'>Learn More &#8594;</button>
                    </div>
                </div>

            </div>
            <div className='md:flex'>
                <div className='lg:w-width lg:h-height xsm:w-full'>
                    <div className='w-wide h-small lg:mt-32 lg:mx-36 lg:py-16 lg:px-10 xsm:w-full lg:text-start xsm:pt-10 xsm:text-center'>
                        <h2 className='lg:text-4xl xsm:text-3xl leading-more-loose font-serif'>Concept 3D Animation</h2>
                    </div>
                    <div className='lg:w-wide lg:h-small xsm:w-full xsm:h-full lg:-mt-2 lg:mx-36 lg:px-10 lg:text-start xsm:px-10 xsm:-mt-10 xsm:text-center'>
                        <p className='text-base font-sans font-light leading-loose'>Pokhara is situated in the very beautiful heart of Nepal and offers many
                            opportunities for tourism.
                        </p>
                    </div>
                    <div className='lg:hidden'>
                        <Image
                            src="/3.png"
                            alt=""
                            width={720}
                            height={439}
                            className="lg:my-20 lg:mx-32 lg:float-right  xsm:p-6"
                        />
                    </div>
                    <div className='w-wider h-small lg:-my-24 lg:mx-36 lg:p-10 xsm:mx-32 xsm:my-10'>
                        <button className='text-white w-36 h-14 bg-blue mr-4'>Learn More &#8594;</button>
                    </div>
                </div>
                <div className='lg:block xsm:hidden'>
                    <Image
                        src="/3.png"
                        alt=""
                        width={720}
                        height={439}
                        className="lg:my-20 lg:mx-32 lg:float-right xsm:-mt-28 xsm:p-6"
                    />
                </div>

            </div>
        </>
    )
}
export default About