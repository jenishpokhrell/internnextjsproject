// import '../globals.css'
import React from 'react'
import Image from 'next/image'
import { FaFacebookF } from 'react-icons/fa'
import { ImTwitter } from 'react-icons/im'
import { AiFillYoutube, AiFillInstagram } from 'react-icons/ai'

const Footer = () => {
    return (
        <div className='lg:w-full lg:h-f-height xsm:h-full bg-black text-white lg:p-20 xsm:p-10'>
            <div className='lg:flex justify-between mb-10'>
                <div>
                    <div className='mb-2'>
                        <Image
                            src="/17.png"
                            alt=""
                            width={145}
                            height={145}
                            className=""
                        />
                    </div>
                    <div className='lg:p-4 lg:py-0 xsm:py-4'>
                        <p className='text-[#CCFF00]'>Noah's Ark Sports Science College</p>
                    </div>
                    <div className='flex'>
                        <span className='text-white lg:p-3 xsm:pr-3'><FaFacebookF size={20} /></span>
                        <span className='text-white lg:p-3 xsm:pr-3'><ImTwitter size={20} /></span>
                        <span className='text-white lg:p-3 xsm:pr-3'><AiFillYoutube size={20} /></span>
                        <span className='text-white lg:p-3 xsm:pr-3'><AiFillInstagram size={20} /></span>
                    </div>
                </div>
                <div className='lg:mt-0 xsm:mt-8'>
                    <div>
                        <h2 className='lg:text-2xl xsm:text-1xl mx-auto leading-more-loose font-serif'>For General Information</h2>
                        <p className='text-[#CCFF00] font-light'>info@noahsarknepal.com</p>
                    </div>
                    <div className='lg:mt-10'>
                        <h2 className='lg:text-2xl xsm:text-1xl mx-auto leading-more-loose font-serif'>For Investment Information</h2>
                        <p className='text-[#CCFF00] font-light'>investment@noahsarknepal.com</p>
                    </div>
                </div>
                <div className='lg:mt-0 xsm:mt-8'>
                    <h2 className='text-2xl mx-auto leading-more-loose font-serif mb-2'>Contact</h2>
                    <p className='font-light'>GM Dr. Ngmar Jang Gurung (Nepal)</p>
                    <p className='font-light'>+977 9810 21 63 53</p>
                    <p className='font-light'>(UK) +44 7787 08 86 05</p>
                    <p className='font-light'>(Now in UK [Since 25, Nov 2020])</p>
                    <br />
                    <p className='font-light'>Dipa Gurung Rai</p>
                    <p className='font-light'>(UK) +44 7428 10 50 17</p>
                </div>
            </div>
            <hr></hr>
        </div>
    )
}

export default Footer