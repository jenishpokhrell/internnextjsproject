import React from 'react'
import Image from 'next/image'

const Gallery = () => {
    return (
        <div className='lg:w-full'>
            <div className='text-center lg:mt-4 lg:mb-10 xsm:my-10'>
                <h2 className='lg:text-4xl xsm:text-3xl leading-more-loose font-serif'>
                    Our Gallery
                </h2>
            </div>
            <div className='lg:grid lg:grid-rows-2 lg:grid-flow-col lg:px-0 xsm:px-6'>
                <div className='lg:row-span-2'>
                    <Image
                        src="/4.png"
                        alt=""
                        width={720}
                        height={720}
                        className=''
                    />
                </div>
                <div className='lg:px-0 lg:pt-0 xsm:px-12 xsm:pt-4'>
                    <Image
                        src="/5.png"
                        alt=""
                        width={360}
                        height={360}
                        className=''

                    />
                </div>
                <div className='lg:px-0 lg:pt-0 xsm:px-12 xsm:pt-4'>
                    <Image
                        src="/6.png"
                        alt=""
                        width={360}
                        height={360}
                        className=''

                        
                    />
                </div>
                <div className='lg:px-0 lg:pt-0 xsm:px-12 xsm:pt-4'>
                    <Image
                        src="/7.png"
                        alt=""
                        width={360}
                        height={360}
                        className=''

                    />
                </div>
                <div className='lg:px-0 lg:pt-0 xsm:px-12 xsm:pt-4'>
                    <Image
                        src="/8.png"
                        alt=""
                        width={360}
                        height={360}
                        className=''

                    />
                </div>
            </div>
        </div>
    )
}

export default Gallery