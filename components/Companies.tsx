import React from 'react'
import Image from 'next/image'

const Companies = () => {
  return (
    <div className='lg:h-height xsm:h-full bg-[#F9F9F9] lg:pb-0 xsm:pb-14'>
      <div className='text-center lg:mt-12 xsm:mt-6'>
        <h2 className='lg:text-4xl xsm:text-2xl leading-more-loose font-serif lg:p-20 xsm:p-10'>Our Companies</h2>
      </div>
      <div className='lg:flex lg:mx-24'>
        <div className='lg:basis-1/4'>
          <div className=' bg-[#FFFFFF] lg:my-0 xsm:my-6 lg:pb-0 xsm:pb-6 lg:w-c-width lg:h-c-height drop-shadow'>
            <div className='flex items-center pt-10'>
              <div className=''>
                <Image
                  src="/12.png"
                  alt=""
                  width={47}
                  height={47}
                  className="ml-20 mr-8"
                />
              </div>
              <div >
                <Image
                  src="/13.png"
                  alt=""
                  width={78}
                  height={41}
                  className=""
                />
              </div>

            </div>
            <div className='p-6'>
              <h3 className='text-center text-1xl'>Noah's Ark Creation Museum Investment Co. Ltd.</h3>
            </div>
            <div className='mx-12'>
              <p className='font-light'>Company Number: <strong className='font-semibold'>13358153</strong></p>
              <p className='font-light'>Company Type: <strong className='font-semibold'>Private Limited Company</strong></p>
              <p className='font-light'>Date: 26 April 2022</p>
            </div>
          </div>
        </div>
        <div className='basis-1/4'>
          <div className=' bg-[#FFFFFF] lg:my-0 xsm:my-6 lg:pb-0 xsm:pb-6 lg:w-c-width lg:h-c-height drop-shadow'>
            <div className='flex items-center pt-10'>
              <div>
                <Image
                  src="/12.png"
                  alt=""
                  width={47}
                  height={47}
                  className="ml-20 mr-8"
                />
              </div>
              <div>
                <Image
                  src="/14.png"
                  alt=""
                  width={37}
                  height={45}
                  className=""
                />
              </div>

            </div>
            <div className='p-6'>
              <h3 className='text-center text-1xl pb-6'>Noah's Ark Sports Science College</h3>
            </div>
            <div className='mx-6'>
              <p className='font-light'>OCR Registration: <strong className='font-semibold'>195883</strong></p>
              <p className='font-light'>Company Type: <strong className='font-semibold'>Lorem Ipsum Pvt. Ltd.</strong></p>
              <p className='font-light'>Date: 26 April 2022</p>
            </div>
          </div>
        </div>
        <div className='basis-1/4'>
          <div className='bg-[#FFFFFF] lg:my-0 xsm:my-6 lg:pb-0 xsm:pb-6 lg:w-c-width lg:h-c-height drop-shadow'>
            <div className='flex items-center pt-10'>
              <div>
                <Image
                  src="/12.png"
                  alt=""
                  width={47}
                  height={47}
                  className="ml-20 mr-8"
                />
              </div>
              <div>
                <Image
                  src="/14.png"
                  alt=""
                  width={37}
                  height={45}
                  className=""
                />
              </div>
            </div>
            <div className='p-6'>
              <h3 className='text-center text-1xl '>Noah's Ark Genesis Creation Museum</h3>
            </div>
            <div className='mx-12'>
              <p className='font-light'>Company Number: <strong className='font-semibold'>13358153</strong></p>
              <p className='font-light'>Company Type: <strong className='font-semibold'>Private Limited</strong></p>
              <p className='font-light'>Date: 26 April 2022</p>
            </div>
          </div>
        </div>
        <div className='basis-1/4'>
          <div className=' bg-[#FFFFFF] lg:my-0 xsm:my-6 lg:pb-0 xsm:pb-6 lg:w-c-width lg:h-c-height drop-shadow'>
            <div className='flex items-center pt-10'>
              <div>
                <Image
                  src="/15.png"
                  alt=""
                  width={47}
                  height={47}
                  className="ml-20 mr-8"
                />
              </div>
              <div>
                <Image
                  src="/14.png"
                  alt=""
                  width={37}
                  height={45}
                  className=""
                />
              </div>
            </div>
            <div className='p-6'>
              <h3 className='text-center text-17'>Noah's Ark Agro Tourism & Homestays</h3>
            </div>
            <div className='mx-12'>
              <p className='font-light italic'>Registration Pending.....</p>             
            </div>
          </div>
        </div>
      
      </div>
    </div>

    
  )
}

export default Companies