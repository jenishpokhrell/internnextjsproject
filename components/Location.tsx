import React from 'react'
import Image from 'next/image'

const Location = () => {
    return (
        <div>
            <Image
                src="/16.png"
                alt=""
                width={1440}
                height={245}
                className="w-full"
            />
        </div>
    )
}

export default Location