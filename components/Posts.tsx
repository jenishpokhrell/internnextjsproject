import React from 'react'
import Image from 'next/image'
import { SiConstruct3 } from 'react-icons/si'
import { VscSymbolOperator } from 'react-icons/vsc'

const Posts = () => {
    return (
        <div>
            <div className='text-center lg:mt-36 xsm:mt-16 lg:px-0 xsm:px-6'>
                <h2 className='lg:text-4xl xsm:text-2xl mx-auto leading-more-loose font-serif'>
                    Noah's Ark Sports Science College Investing
                </h2>
            </div>
            <div className='lg:flex'>
                <div>
                    <Image
                        src="/11.png"
                        alt=""
                        width={596}
                        height={455}
                        className="lg:my-20 lg:mx-32 lg:px-0 xsm:my-10 xsm:px-6"
                    />
                </div>
                <div className='lg:px-0 xsm:px-6'>
                    <div className='lg:flex my-24 lg:-mx-20 items-center'>
                        <div>
                            <div className='flex mb-4'>
                                <div className='mr-4'><SiConstruct3 size={40} /></div>
                                <div className='lg:text-3xl xsm:text-2xl text-[#120085]'>Construction Projection</div>
                            </div>
                            <div className='lg:w-wide xsm:w-full'>
                                <p className='text-base font-sans font-light leading-loose'>Pokhara is situated in the very beautiful heart of Nepal and offers many opportunities for tourism.</p>
                            </div>
                        </div>
                        <div className='ml-6'>
                            <div className='h-small items-center lg:border border-[#EDEDED] border-solid'></div>
                        </div>
                        <div className='lg:ml-10 lg:mt-0 xsm:-mt-28'>
                            <p className='text-base font-sans font-light leading-loose'>Learn More &#8594;</p>
                        </div>
                    </div>
                    <div className='lg:flex mt-16 lg:-mx-20 items-center'>
                        <div>
                            <div className='flex mb-4'>
                                <div className='mr-4'><VscSymbolOperator size={40} /></div>
                                <div className='lg:text-3xl xsm:text-2xl text-[#120085]'>Financial Projection</div>
                            </div>
                            <div className='lg:w-wide xsm:w-full'>
                                <p className='text-base font-sans font-light leading-loose'>Tourism opportunities include boating, cycling, homestay in traditional
                                    village, paragliding, ultralight flights, zip were, bungee jumping and, of course, trekking.</p>
                            </div>
                        </div>
                        <div className='ml-6'>
                            <div className='h-small items-center lg:border border-[#EDEDED] border-solid'></div>
                        </div>
                        <div className='lg:ml-10 lg:mt-0 xsm:-mt-28'>
                            <p className='text-base font-sans font-light leading-loose'>Learn More &#8594;</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Posts