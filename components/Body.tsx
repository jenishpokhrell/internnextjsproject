import React from 'react'
import Image from 'next/image'

const Body = () => {
    return (
        <div className='md:w-full'>
            <div>
                <Image
                    src="/21.png"
                    alt=""
                    width={1440}
                    height={720}
                    className="md:w-full md:h-full"
                />
            </div>
        </div>
    )
}

export default Body
