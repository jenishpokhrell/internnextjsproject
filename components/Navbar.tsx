"use client"
import React, { useState } from 'react'
import Image from 'next/image'
import { HiMenu } from 'react-icons/hi'
import { MdClose } from 'react-icons/md'
import { Link } from 'react-scroll/modules'


interface NavItem {
    label: string,
    page: string,
}

// const [navbar, setNavBar] = useState(false)

const NAV_ITEMS: Array<NavItem> = [
    {
        label: 'About',
        page: 'about',
    },
    {
        label: 'Videos',
        page: 'videos',
    },
    {
        label: 'Images',
        page: 'images',
    },
    {
        label: 'Investing',
        page: 'investing',
    },
    {
        label: 'Companies',
        page: 'companies',
    },
    {
        label: 'Tourism',
        page: 'tourism',
    },
    {
        label: 'Location',
        page: 'location',
    },
]

const Navbar = () => {
    const [navbar, setNavbar] = useState(false)
    return (
        <header className='lg:w-full h-20 mx-auto px-4 bg-white border border-solid sticky z-20'>
            <div className='justify-between md:items-center md:flex'>
                <div>
                    <div className='flex items-center justify-between md:block'>
                        <div>
                            <Image
                                src="/1.png"
                                alt=""
                                width={50}
                                height={51}
                                className="my-3 mx-10"
                            />
                        </div>
                        <div className='md:hidden '>
                            <button className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
                                onClick={() => setNavbar(!navbar)}>
                                {navbar ? <MdClose size={30} /> : <HiMenu size={30} />}
                            </button>
                        </div>
                    </div>
                </div>
                <div className=''>
                    <div className={`flex-1 justify-self-center pb-3 md:block md:pb-0 md:mt-0 ${navbar ? "block" : "hidden"
                        }`}>
                        <div className='justify-center items-center md:flex md:space-x-6 md:space-y-0 bg-white'>
                            {/* <div className=''> */}
                                {NAV_ITEMS.map((item, idx) => {
                                    return (
                                        <Link
                                            key={idx}
                                            to={item.page}
                                            className={
                                                "pl-10 pt-2 block lg:inline-block text-black  hover:text-neutral-500 tracking-widest font-sans text-base font-light"
                                            }
                                            activeClass="active"
                                            spy={true}
                                            smooth={true}
                                            offset={-100}
                                            duration={500}
                                            onClick={() => setNavbar(!navbar)}
                                        >
                                            {item.label}
                                        </Link>
                                    )
                                })}
                            {/* </div> */}
                            <div className='pt-3 pb-3 pl-10'>
                                <button
                                    className=' text-white w-32 h-12 bg-blue mr-4 transition-all hover:bg-transparent
                                 hover:text-black hover:border'>Contact
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}

export default Navbar