import About from "@/components/About";
import Blog from "@/components/Blog";
import Body from "@/components/Body";
import Companies from "@/components/Companies";
import Gallery from "@/components/Gallery";
import Location from "@/components/Location";
import Posts from "@/components/Posts";


export default function Home() {
  return (
    <div >
      <Body />
      <About/>
      <Gallery/>
      <Blog/>
      <Posts/>
      <Companies/>
      <Location/>
    </div>
  )
}
